# Quote Database

A simple react-based app to collect and manage quotes.

## Development

In one terminal, run:

    yarn run server

In a second terminal, run:

    yarn start

## Learning Goals

  - [ ] React Hooks
  - [ ] React Suspense
  - [ ] Non-graphql API stuff

## First MVP

1. Display a list of quotes in quotes.json file.
2. Add a quote to the file.

## TODOS

  - [ ] modify App.js to display list of quotes from server
  - [ ] connect front and back ends (proxy?)
  - [ ] add storybook
  - [ ] define major features and UX
  - [ ] design app
  - [ ] structure layout
  - [ ] create CSS for layout
  - [ ] build component library with stories
  - [ ] build RESTful API hooks, learn React Suspense
